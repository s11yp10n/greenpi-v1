# GreenPI version 1.0 #

A REST API server side application using Express+JWT Simple + Sequel ORM

### End points ###
-----

It handles **login** authentication and act as **data teller** for an authenticated user

  `http://localhost:3000/login`

  `http://localhost:3000/household/<username>/?access_token=<token>`

  `http://localhost:3000/household/today/<username>/?access_token=<token>`

  `http://localhost:3000/household/week/<username>/?access_token=<token>`

  `http://localhost:3000/household/month/<username>/?access_token=<token>`

  `http://localhost:3000/household/year/<username>/?access_token=<token>`


### Configuration and Installation ###
------

Check if `nodejs` and `npm` installed 

Configure your database at `GreenPI-v1/config/config.json` 

 
```
{
  "development": {
      "username": "postgres",
      "password": "$million",
      "database": "green_dev",
      "host": "localhost",
      "port" : 5432,
      "dialect": "postgres",
      "pool": {
           "maxConnections": 99,
           "minConnections": 5,
           "maxIdleTime": 30
        }
  },

  "test": {
    ....
    }
  },

  "production": {
    ...
    }
  }
}

```


`cd GreenPI-v1 && npm install && node app.js` or `npm install && node app.js` (if you already inside)

-----

### Test ###
-----

In **curl** ` curl -vsb -I -H "username:awesomeUser" -H "password:complexPass" http://localhost:3000/login` and get the  token. It looks something like this :

```
* Hostname was NOT found in DNS cache
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 3000 (#0)
> GET /login HTTP/1.1
> User-Agent: curl/7.38.0
> Host: localhost:3000
> Accept: */*
> username:awesomeUser
> password:complexPass
> 
< HTTP/1.1 200 OK
< X-Powered-By: Express
< Content-Type: application/json; charset=utf-8
< Content-Length: 241
< ETag: W/"f1-ebed84b8"
< Date: Sun, 24 May 2015 15:49:09 GMT
< Connection: keep-alive
< 
{
  "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOjEsImV4cCI6MTQzMjU2ODk0OTcxM30.UHVhFLlyuk7Bi_LWztoP0eV4Npx_6ZnDjikQL6mxe8k",
  "expires": 1432568949713,
  "user": [
    {
      "id": 1,
      "username": "awesomeUser"
    }
  ]
* Connection #0 to host localhost left intact

```

In **postman** view the data for the above authenticated user with `http://localhost:3000/household/awesomeUser?access_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOjEsImV4cCI6MTQzMjU2MjU1NTExNn0.HAgRFai_2rDRh79csOJ2QXNNULVqcQenyNLxx9kEEto`

### Some Results ###

**`/household/today/<userName>?`** endpoint:  `http://localhost:3000/household/today/awesomeUser?access_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOjEsImV4cCI6MTQzMjc0NDc3NDQ1N30.ADAk8RlYN1W_gSk5S7EWiZxKLfvKUsZ8xsBl_bXgd9U`

Would result in total cost of the day until the time :

```
[
  {
    "cost_in_sek": 345161
  }
]
```

**`/household/week/<userName>?`** endpoint:  `http://localhost:3000/household/week/awesomeUser?access_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOjEsImV4cCI6MTQzMjc0NDc3NDQ1N30.ADAk8RlYN1W_gSk5S7EWiZxKLfvKUsZ8xsBl_bXgd9U`

Would result in cost distribution of the week :


```
[
  {
    "cost": 71.67
  },
  {
    "cost": 152.86
  },
  {
    "cost": 33.11
  },
  ...
]
```

**`/household/month/<userName>?`** endpoint:  `http://localhost:3000/household/month/awesomeUser?access_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOjEsImV4cCI6MTQzMjc0NDc3NDQ1N30.ADAk8RlYN1W_gSk5S7EWiZxKLfvKUsZ8xsBl_bXgd9U`

Would result in monthly energy consumption and monthly cost:

```
[
  {
    "monthly_consumption": 1560.78,
    "monthly_cost": 345161
  }
]
```

**`/household/year/<userName>?`** endpoint:  `http://localhost:3000/household/year/awesomeUser?access_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOjEsImV4cCI6MTQzMjc0NDc3NDQ1N30.ADAk8RlYN1W_gSk5S7EWiZxKLfvKUsZ8xsBl_bXgd9U`

Would result in annual energy consumption and cost:

```
[
  {
    "annual_consumption": 1560.78,
    "annual_cost": 345161
  }
]
```

### Resolved Bugs ###
With username any authenticated user could access others data:
Problem found in 'lib/jwauth.js' file and addition of 2 lines protect such access now

```
module.exports = function(req, res, next){

	var token = (req.body && req.body.access_token) || parsed_url.query.access_token || req.headers["x-access-token"];

	if (token) {
		try {
            var attemptUser = req.param('userName');  // Get the typed username in the url
			...
            models.user.findOne({ where: { id: decoded.iss},attributes: ['username'] }).then(function(user) {
                // check the authenticated name matches with attempting user
                if (attemptUser !== user.username) res.send("You are not the owner ! Supply your username");

                else if (user){
                    req.user = user;
                    return next();
                }
            });

		}
		...
	}
	...
}

```