/**
 * Require in the other controllers
 */
var directory = require('./directory.js')
var auth = require('./auth.js')


module.exports.set = function(app) {
	
	//Now set the routes from the other controllers
	directory.set(app)
	auth.set(app)

}

