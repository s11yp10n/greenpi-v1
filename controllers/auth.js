var express = require('express')
var jwt = require('jwt-simple');
var moment = require('moment');
var models  = require('../models');

module.exports.set = function(app) {

	app.get('/login', express.bodyParser(), function(req, res){
		
		if (req.headers.username && req.headers.password) {

            models.user.findOne({ where: { username: req.headers.username},attributes: ['id','username', 'password'] }).then(function(user) {

             if(user) {
                 models.user.comparePassword(req.headers.password, user.password.trim(), function (err, isMatch) {
                     if (err) {
                         // an error has occured checking the password. For simplicity, just return a 401
                         res.send('Authentication error', 401)
                     }
                     if (isMatch) {
                         // Great, user has successfully authenticated, so we can generate and send them a token.
                         //1 day of token
                         var expires = moment().add('days', 1).valueOf()
                         var token = jwt.encode(
                             {
                                 iss: user.id,
                                 exp: expires
                             },
                             app.get('jwtTokenSecret')
                         );
                         res.json({
                             token: token,
                             expires: expires,
                             user: models.user.filtered(user)
                         });
                     } else {
                         // The password is wrong...
                         res.send('Authentication error', 401)
                     }
                 });

             }
              else
             {
                 // User Non-existent
                 res.send('Authentication error', 401)
             }

            });

            //find by success

		} else {
			// No username provided, or invalid POST request. For simplicity, just return a 401
			res.send('Authentication error', 401)
		}
	});

}

