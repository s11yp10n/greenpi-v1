/**
 * Service resources 
 */

var household = require('./household.js');


var models  = require('../models');

module.exports.set = function(app,express,jwtauth, requireAuth) {
    // Now set the servicers
    household.set(app,express,jwtauth, requireAuth,models);

}