module.exports.set = function(app,express,jwtauth, requireAuth,models) {
    /**
     * @api {get} /houldhold/:userName Request Household usage for an User
     * @apiName GetHouseholdActivity
     * @apiGroup Household
     *
     * @apiParam {String} User Name.
     *
     * @apiSuccess {Collection} Collection of Household activities.
     *
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * [{
     *  "id": 8,
        * },
     * {
     *  "id": 9,
       * }]
     *
     * @apiError Empty collection.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "error": "User not found"
     *     }
     */
    app.get('/household/:userName', express.bodyParser(), jwtauth, requireAuth, function(req, res){

       var userName = req.param('userName');

        models.sequelize.query('select ht.* from users u left outer join "user_household" uh ' +
                               ' on u.id = uh.user_id left outer join household_timeseries ht ' +
                               ' on uh.household_id = ht.household_id where u.username=?'
            , { replacements: [userName], type: models.sequelize.QueryTypes.SELECT })
            .then(function (htseries) {

                res.send(htseries);

            })


    });



    // Other customized details fetching such as /today, /week, /month, /year

    app.get('/household/today/:userName', express.bodyParser(), jwtauth, requireAuth, function(req, res){

        var userName = req.param('userName');

        models.sequelize.query('select sum(ht.cost) as cost_in_sek from users u left outer join "user_household" uh ' +
            ' on u.id = uh.user_id left outer join household_timeseries ht ' +
            ' on uh.household_id = ht.household_id where u.username=?'
            , { replacements: [userName], type: models.sequelize.QueryTypes.SELECT })
            .then(function (htseries) {

                res.send(htseries);

            })


    });

    app.get('/household/week/:userName', express.bodyParser(), jwtauth, requireAuth, function(req, res){

        var userName = req.param('userName');

        models.sequelize.query('select ht.cost from users u left outer join "user_household" uh ' +
            ' on u.id = uh.user_id left outer join household_timeseries ht ' +
            ' on uh.household_id = ht.household_id where u.username=?'
            , { replacements: [userName], type: models.sequelize.QueryTypes.SELECT })
            .then(function (htseries) {

                res.send(htseries);

            })


    });


    app.get('/household/month/:userName', express.bodyParser(), jwtauth, requireAuth, function(req, res){

        var userName = req.param('userName');

        models.sequelize.query('select sum(ht.consumption) as monthly_consumption, sum(ht.cost) as monthly_cost  from users u left outer join "user_household" uh ' +
            ' on u.id = uh.user_id left outer join household_timeseries ht ' +
            ' on uh.household_id = ht.household_id where u.username=?'
            , { replacements: [userName], type: models.sequelize.QueryTypes.SELECT })
            .then(function (htseries) {

                res.send(htseries);

            })


    });

    app.get('/household/year/:userName', express.bodyParser(), jwtauth, requireAuth, function(req, res){

        var userName = req.param('userName');

        models.sequelize.query('select sum(ht.consumption) as annual_consumption, sum(ht.cost) as annual_cost from users u left outer join "user_household" uh ' +
            ' on u.id = uh.user_id left outer join household_timeseries ht ' +
            ' on uh.household_id = ht.household_id where u.username=?'
            , { replacements: [userName], type: models.sequelize.QueryTypes.SELECT })
            .then(function (htseries) {

                res.send(htseries);

            })


    });



}

