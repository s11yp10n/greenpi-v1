"use strict";

// session table is created
module.exports = function(sequelize, DataTypes) {
  var Session = sequelize.define("Session", {
    token: DataTypes.STRING

  }, {
    classMethods: {

    }
  });

  return Session;
};
