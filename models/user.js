
"use strict";
//user model defined
module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define("user", {
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    email:DataTypes.STRING,
    auth_token:  DataTypes.STRING
  }, {
    classMethods: {
   filtered: function(user) {
        return [{id:user.id,username:user.username.trim()}]
      },
      comparePassword :function(candidatePassword,userpass, cb) {
           if(candidatePassword == userpass) return cb(null, true);
                else cb(true);
      }
    }
  });


  return User;
};
