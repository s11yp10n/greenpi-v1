/**
 * Green Web Services
 */
var express = require('express')
var colors = require('colors')
var url = require('url')
var jwt = require('jwt-simple');
var models = require("./models");

/**
 * THe JWT middleware
 */
var jwtauth = require('./lib/jwtauth')


/**
 * Create the express app
 * NOTE: purposely not using var so that app is accesible in modules.
 */
app = express()

/**
 * Set the secret for encoding/decoding JWT tokens
 */
app.set('jwtTokenSecret', 'greenws')

/**
 * A simple middleware to restrict access to authenticated users.
 */
var requireAuth = function(req, res, next) {
	if (!req.user) {
		res.end('Not authorized', 401)
	}	else {
		next()
	}
}

/**
 * Load up the controllers
 */
var controllers = require('./controllers')
controllers.set(app)

/**
 * Start listening
 */


app.set('port', process.env.PORT || 3000);


//models.sequelize.sync().success(function () {

    models.sequelize.sync().then(function() {

    var server = app.listen(app.get('port'), function() {
        console.log('Listening on port %d'.green, server.address().port)
        //debug('Express server listening on port ' + server.address().port);
    });
});


/**
 * Load up the servicers
 */
var servicers = require('./servicers')
servicers.set(app,express, jwtauth, requireAuth)

